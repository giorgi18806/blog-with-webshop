@extends('layouts.master')

@section('content')
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('{{ asset('assets/img/post-bg.jpg') }}')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="post-heading">
                        <h1>{{ $post->title }}</h1>
{{--                        <h2 class="subheading">Problems look mighty small from 150 miles up</h2>--}}
                        <span class="meta">
                                    Posted by
                                    <a href="#!">{{ $post->user->name }}</a>
                                    on {{ date_format($post->created_at, 'F d, Y') }}
                                </span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Post Content-->
    <article class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    {!! nl2br($post->content) !!}
                </div>
            </div>

            <div class="comments">
                <hr>
                <h2>Comments</h2>
                <hr>
                @foreach($post->comments as $comment)
                    <p>{{ $comment->content }}</p>
                    <p style="font-size: 14px;"><small>by {{ $comment->user->name }}, on {{ date_format($comment->created_at, 'F d, Y') }}</small></p>
                    <hr>
                @endforeach
                @if(Auth::check())
                    <form action="{{ route('user.comment.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <textarea class="form-control" name="content" placeholder="Comment..." cols="30" rows="5"></textarea>
                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Make Comment</button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </article>
@endsection
