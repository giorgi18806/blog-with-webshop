@extends('layouts.admin')

@section('title', 'Admin Users')

@section('content')
    <div class="content">
        <div class="card">
            <div class="caUsersrd-header bg-light">
                Admin Users
            </div>
            @if(Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Posts</th>
                            <th>Comments</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td class="text-nowrap">{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->posts->count() }}</td>
                                <td>{{ $user->comments->count() }}</td>
                                <td>{{ \Carbon\Carbon::parse($user->created_at)->diffForHumans() }}</td>
                                <td>{{ \Carbon\Carbon::parse($user->updated_at)->diffForHumans() }}</td>
                                <td>
                                    <div class="row">
                                        <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-warning btn-sm mr-2"><i class="fa fa-pencil-alt"></i></a>
                                        <form action="{{ route('admin.user.delete', $user->id) }}" id="deleteUser-{{ $user->id }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                        <a href="#" class="btn btn-danger" onclick="document.getElementById('deleteUser-{{ $user->id }}').submit()"><i class="fa fa-trash-alt"></i></a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
