@extends('layouts.admin')

@section('title', 'Admin Products')

@section('content')
    <div class="content">
        <div class="card">
            <div class="card-header bg-light">
                Admin Products
                <a href="{{ route('admin.products.create') }}" class="btn btn-primary btn-sm float-right">Create New Product</a>
            </div>
            @if(Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
            @endif
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Thumbnail</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td><img src="{{ asset($product->thumbnail) }}" width="100" alt="{{ $product->title }}"></td>
                                <td class="text-nowrap"><a href="{{ route('admin.products.edit', $product) }}">{{ $product->title }}</a></td>
                                <td>{{ $product->description }}</td>
                                <td>{{ $product->price }} USD</td>
                                <td>
                                    <div class="row">
                                        <a href="{{ route('admin.products.edit', $product) }}" class="btn btn-warning btn-sm mr-2"><i class="fa fa-pencil-alt"></i></a>
                                        <button type="button" class="btn btn-danger bt-sm" data-toggle="modal" data-target="#deleteProductModal-{{ $product->id }}"><i class="fa fa-trash-alt"></i></button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @foreach($products as $product)
        <!-- Modal -->
        <div class="modal fade" id="deleteProductModal-{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">You are about to delete {{ $product->title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        Are you sure?
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No, keep the product</button>
                        <form action="{{ route('admin.products.delete', $product) }}"
                              id="deleteProduct-{{ $product->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary">Yes, delete the product</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
