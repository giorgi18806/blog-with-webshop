@extends('layouts.admin')

@section('title', 'Edit Product')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Edit Product
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        {{--                        @if($errors->any)--}}
                        {{--                            <div class="alert alert-danger">--}}
                        {{--                                <ul>--}}
                        {{--                                    @foreach($errors->all() as $error)--}}
                        {{--                                        <li>{{ $error }}</li>--}}
                        {{--                                    @endforeach--}}
                        {{--                                </ul>--}}
                        {{--                            </div>--}}
                        {{--                        @endif--}}
                        <form action="{{ route('admin.products.update', $product) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="thumbnail" class="form-control-label">Thumbnail</label>
                                            <input type="file" id="thumbnail" name="thumbnail" class="form-control">
                                            @error('thumbnail') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                        <img src="{{ asset($product->thumbnail) }}" width="100" alt="{{ $product->title }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="title" class="form-control-label">Title</label>
                                            <input id="title" name="title" class="form-control" value="{{ $product->title }}" placeholder="Product title" value="{{ old('title') }}">
                                            @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="description" class="form-control-label"> Description</label>
                                            <textarea id="description" name="description" class="form-control" placeholder="Product description" cols="30" rows="10">{{ $product->description }}</textarea>
                                            @error('description') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="price" class="form-control-label">Price</label>
                                            <input id="price" name="price" class="form-control" placeholder="Product price"  value="{{ number_format($product->price, 2) }}">
                                            @error('price') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
