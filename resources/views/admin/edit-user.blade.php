@extends('layouts.admin')

@section('title', 'Editing ' . $user->name)

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Editing {{ $user->name }}
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
{{--                        @if($errors->any)--}}
{{--                            <div class="alert alert-danger">--}}
{{--                                <ul>--}}
{{--                                    @foreach($errors->all() as $error)--}}
{{--                                        <li>{{ $error }}</li>--}}
{{--                                    @endforeach--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        @endif--}}
                        <form action="{{ route('admin.user.update', $user) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="name" class="form-control-label">Name</label>
                                            <input id="name" name="name" class="form-control" value="{{ $user->name }}">
                                            @error('name') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="form-control-label">Email</label>
                                            <input type="email" id="email" name="email" class="form-control" value="{{ $user->email }}">
                                            @error('email') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="author" class="form-control-label">Permissions</label>
                                            <p><input type="checkbox" name="author" value="1" {{ $user->author == true ? 'checked' : '' }}> Author</p>
                                            <p><input type="checkbox" name="admin" value="1" {{ $user->admin == true ? 'checked' : '' }}> Admin</p>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
