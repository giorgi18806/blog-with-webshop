@extends('layouts.admin')

@section('title', 'Create New Post')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header bg-light">
                            Create New Post
                        </div>
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
{{--                        @if($errors->any)--}}
{{--                            <div class="alert alert-danger">--}}
{{--                                <ul>--}}
{{--                                    @foreach($errors->all() as $error)--}}
{{--                                        <li>{{ $error }}</li>--}}
{{--                                    @endforeach--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        @endif--}}
                        <form action="{{ route('author.post.store') }}" method="POST">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="title" class="form-control-label">Title</label>
                                            <input id="title" name="title" class="form-control" placeholder="Post title" value="{{ old('title') }}">
                                            @error('title') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-4">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="content" class="form-control-label"> Content</label>
                                            <textarea id="content" name="content" class="form-control" placeholder="Post content" cols="30" rows="10"></textarea>
                                            @error('content') <span class="text-danger error"><small>{{ $message }}</small></span>@enderror
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
