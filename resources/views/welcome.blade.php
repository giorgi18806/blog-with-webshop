@extends('layouts.master')

@section('content')
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('{{ asset('assets/img/home-bg.jpg') }}')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <h1>Giorgi's Blog</h1>
                        <span class="subheading">{{remove_spaces('Personal Blog Powered by Laravel')}}</span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main Content-->
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                @foreach($posts as $post)
                    <!-- Post preview-->
                    <div class="post-preview">
                        <a href="{{ route('single-post', $post) }}">
                            <h2 class="post-title">{{ $post->title }}</h2>
{{--                            <h3 class="post-subtitle">Problems look mighty small from 150 miles up</h3>--}}
                        </a>
                        <p class="post-meta">Posted by
                            <a href="#!">{{ $post->user->name }}</a>
                            on {{ date_format($post->created_at, 'F d, Y') }}
                            |
                            <i class="fa fa-comment" aria-hidden="true"></i>{{ $post->comments->count() }}
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                @endforeach
                {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection
