@extends('layouts.master')

@section('content')
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('{{ asset('assets/img/home-bg.jpg') }}')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <h1>Shop</h1>
                        <span class="subheading">Super Cool T-Shirts</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
            @foreach($products as $product)
                <!-- Post preview-->
                    <div class="post-preview">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="{{ asset($product->thumbnail) }}" width="100" alt="{{ $product->title }}">
                            </div>
                            <div class="col-md-9">
                                <a href="{{ route('shop.show', $product) }}">
                                    <h2 class="post-title">{{ $product->title }}</h2>
                                    {{--                            <h3 class="post-subtitle">Problems look mighty small from 150 miles up</h3>--}}
                                </a>
                                <p class="post-meta">${{ number_format($product->price, 2) }}</p>
                            </div>
                        </div>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                @endforeach
                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection
