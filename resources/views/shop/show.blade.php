@extends('layouts.master')

@section('content')
    <!-- Page Header-->
    <header class="masthead" style="background-image: url('{{ asset('assets/img/home-bg.jpg') }}')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="site-heading">
                        <h1>{{ $product->title }}</h1>
                        <span class="subheading">Super Cool T-Shirts</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <img src="{{ asset($product->thumbnail) }}" width="650" alt="{{ $product->title }}">
            </div>
            <div class="col-md-6">
                <h2>{{ $product->title }}</h2>
                <hr>
                <b>{{ $product->price }} USD</b>
                <br>
                <a href="{{ route('shop.order', $product) }}" class="btn btn-primary">Checkout with PayPal</a>
            </div>
        </div>
    </div>
@endsection
