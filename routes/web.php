<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\User\PublicController;

Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('post/{post}', [PublicController::class, 'singlePost'])->name('single-post');
Route::get('about', [PublicController::class, 'about'])->name('about');
Route::get('contact', [PublicController::class, 'contact'])->name('contact');
Route::get('contact-form', [PublicController::class, 'contactForm'])->name('contact-form');


Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');


Route::prefix('user')->group(function () {
    Route::get('dashboard', [UserController::class, 'dashboard'])->name('user.dashboard');
    Route::get('comments', [UserController::class, 'comments'])->name('user.comments');
    Route::post('comment/create', [UserController::class, 'store'])->name('user.comment.store');
    Route::delete('comment/{id}/delete', [UserController::class, 'commentDelete'])->name('user.comment.delete');
    Route::get('profile', [UserController::class, 'profile'])->name('user.profile');
    Route::post('profile', [UserController::class, 'profilePost'])->name('user.profile-post');
});

Route::prefix('author')->group(function () {
    Route::get('dashboard', [AuthorController::class, 'dashboard'])->name('author.dashboard');
    Route::get('posts', [AuthorController::class, 'posts'])->name('author.posts');
    Route::get('post/create', [AuthorController::class, 'create'])->name('author.post.create');
    Route::post('post/store', [AuthorController::class, 'store'])->name('author.post.store');
    Route::get('post/{id}/edit', [AuthorController::class, 'edit'])->name('author.post.edit');
    Route::put('post/{id}/update', [AuthorController::class, 'update'])->name('author.post.update');
    Route::delete('post/{id}/delete', [AuthorController::class, 'delete'])->name('author.post.delete');
    Route::get('comments', [AuthorController::class, 'comments'])->name('author.comments');
});

//admin
Route::prefix('admin')->group(function () {
    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
    Route::get('posts', [AdminController::class, 'posts'])->name('admin.posts');
    Route::get('post/{id}/edit', [AdminController::class, 'edit'])->name('admin.post.edit');
    Route::put('post/{id}/update', [AdminController::class, 'update'])->name('admin.post.update');
    Route::delete('post/{post}/delete', [AdminController::class, 'delete'])->name('admin.post.delete');
    Route::get('comments', [AdminController::class, 'comments'])->name('admin.comments');
    Route::delete('comment/{comment}/delete', [AdminController::class, 'deleteComment'])->name('admin.comment.delete');
    Route::get('users', [AdminController::class, 'users'])->name('admin.users');
    Route::get('user/{user}/edit', [AdminController::class, 'editUser'])->name('admin.user.edit');
    Route::put('user/{user}/update', [AdminController::class, 'updateUser'])->name('admin.user.update');
    Route::delete('user/{user}/delete', [AdminController::class, 'deleteUser'])->name('admin.user.delete');

    //product
    Route::get('products', [AdminController::class, 'products'])->name('admin.products');
    Route::get('products/create', [AdminController::class, 'productsCreate'])->name('admin.products.create');
    Route::post('products/store', [AdminController::class, 'productsStore'])->name('admin.products.store');
    Route::get('products/{product}/edit', [AdminController::class, 'productsEdit'])->name('admin.products.edit');
    Route::put('products/{product}/update', [AdminController::class, 'productsUpdate'])->name('admin.products.update');
    Route::delete('products/{product}/delete', [AdminController::class, 'productsDelete'])->name('admin.products.delete');
});

//shop
Route::prefix('shop')->group(function() {
    Route::get('/', [ShopController::class, 'index'])->name('shop.index');
    Route::get('product/{product}/show', [ShopController::class, 'show'])->name('shop.show');
    Route::get('product/{product}/order', [ShopController::class, 'order'])->name('shop.order');
    Route::get('product/{product}/execute', [ShopController::class, 'execute'])->name('shop.execute');
});
