<?php

namespace App\Services;

use App\Charts\DashboardChart;
use App\Models\Comment;
use App\Models\Post;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ChartService
{
    public static function generateChart(User $user, $status, $isAdmin = false)
    {
        $chart = new DashboardChart();
        $days = self::generateDateRange(Carbon::now()->subDays(30), Carbon::now());
        $data = [];

        foreach($days as $day) {
            if($isAdmin == true) {
                $data[] = Post::whereDate('created_at', $day)->count();
            }elseif($status == 'Comment') {
                $data[] = Comment::whereDate('created_at', $day)->where('user_id', $user->id)->count();
            }elseif($status == 'Post') {
                $data[] = Post::whereDate('created_at', $day)->where('user_id', $user->id)->count();
            }
        }

        $chart->dataset($status, 'line', $data);
        $chart->labels($days);

        return $chart;
    }

    public static function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];
        for($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }
}
