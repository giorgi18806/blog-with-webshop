<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StorePostRequest;
use App\Models\Comment;
use App\Models\Post;
use App\Services\ChartService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkRole:author');
    }

    public function dashboard()
    {
        $post_ids = Post::where('user_id', Auth::id())->pluck('id')->toArray();
        $all_comments = Comment::whereIn('post_id', $post_ids)->get();
        $today_comments = $all_comments->where('created_at', '>=', Carbon::today())->count();

        $chart = ChartService::generateChart(Auth::user(), 'Post');

        return view('author.dashboard', compact('all_comments', 'today_comments', 'chart'));
    }

    public function posts()
    {
        return view('author.posts');
    }

    public function comments()
    {
        $post_ids = Post::where('user_id', Auth::id())->pluck('id')->toArray();
        $comments = Comment::whereIn('post_id', $post_ids)->get();
        return view('author.comments', compact('comments'));
    }

    public function create()
    {
        return view('author.create');
    }

    public function store(StorePostRequest $request)
    {
        $post = new Post();
        $post->user_id = Auth::id();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();

        session()->flash('success', 'Post has been created successfully');
        return redirect()->route('author.posts');
    }

    public function edit($id)
    {
        $post = Post::where('id', $id)->where('user_id', Auth::id())->first();
        return view('author.edit', compact('post'));
    }

    public function update(StorePostRequest $request, $id)
    {
        $post = Post::where('id', $id)->where('user_id', Auth::id())->first();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();

        session()->flash('success', 'Post has been updated successfully');
        return redirect()->route('author.posts');
    }

    public function delete($id)
    {
        $post = Post::where('id', $id)->where('user_id', Auth::id())->first();
        if($post) {
            $post->delete();
            return back()->with('success', 'Post has been deleted successfully');
        }
        return back()->with('error', 'You have no permission to delete this post');;
    }
}
