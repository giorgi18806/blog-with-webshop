<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Requests\StorePostRequest;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Product;
use App\Services\ChartService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('checkRole:admin');
    }

    public function dashboard()
    {
        $posts = Post::all();
        $comments = Comment::all();
        $users = User::all();

        $chart = ChartService::generateChart(Auth::user(), 'Post', true);

        return view('admin.dashboard', compact('posts', 'comments', 'users', 'chart'));
    }

    public function posts()
    {
        $posts = Post::all();
        return view('admin.posts', compact('posts'));
    }

    public function comments()
    {
        $comments = Comment::all();
        return view('admin.comments', compact('comments'));
    }

    public function edit($id)
    {
        $post = Post::where('id', $id)->first();
        return view('admin.edit', compact('post'));
    }

    public function update(StorePostRequest $request, $id)
    {
        $post = Post::where('id', $id)->first();
        $post->title = $request['title'];
        $post->content = $request['content'];
        $post->save();

        session()->flash('success', 'Post has been updated successfully');
        return redirect()->route('admin.posts');
    }

    public function delete(Post $post)
    {
        $post->delete();
        return back()->with('success', 'Post has been deleted successfully');
    }

    public function deleteComment(Comment $comment)
    {
        $comment->delete();
        return back()->with('success', 'Comment has been deleted successfully');
    }

    public function users()
    {
        $users = User::all();
        return view('admin.users', compact('users'));
    }

    public function editUser(User $user)
    {
        return view('admin.edit-user', compact('user'));
    }

    public function updateUser(ProfileUpdateRequest $request, User $user)
    {
        $user->name = $request->name;
        $user->email = $request->email;

        if($request->author == 1) {
            $user->author = true;
        }
        if($request->admin == 1) {
            $user->admin = true;
        }

        $user->save();

        session()->flash('success', 'User has been updated successfully');
        return redirect()->route('admin.users');
    }

    public function deleteUser(User $user)
    {
        $user->delete();
        return back()->with('success', 'User has been deleted successfully');
    }

    public function products()
    {
        $products = Product::all();
        return view('admin.product.index', compact('products'));
    }

    public function productsCreate()
    {
        return view('admin.product.create');
    }

    public function productsStore(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'thumbnail' => 'required|file',
            'description' => 'required|',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ]);

        $product = new Product();
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;

        $thumbnail = $request->file('thumbnail');

        $fileName = $thumbnail->getClientOriginalName();
        $fileExtension = $thumbnail->getClientOriginalExtension();

        $thumbnail->move('product-images', $fileName);
        $product->thumbnail = 'product-images/' . $fileName;

        $product->save();

        session()->flash('success', 'Product has been created successfully');
        return redirect()->route('admin.products');
    }

    public function productsEdit(Product $product)
    {
        return view('admin.product.edit', compact('product'));
    }

    public function productsUpdate(Request $request, Product $product)
    {
        $this->validate($request, [
            'title' => 'required|string',
            'thumbnail' => 'nullable|file',
            'description' => 'required|',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        ]);

        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;

        if($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $fileName = $thumbnail->getClientOriginalName();
            $thumbnail->move('product-images', $fileName);
            $product->thumbnail = 'product-images/' . $fileName;
        }
        $product->save();

        session()->flash('success', 'Product has been updated successfully');
        return redirect()->route('admin.products');
    }

    public function productsDelete(Product $product)
    {
        $product->delete();
        return back()->with('success', 'Product has been deleted successfully');
    }
}
