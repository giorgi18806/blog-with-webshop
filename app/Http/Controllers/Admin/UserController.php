<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Comment;
use App\Services\ChartService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Profiler\Profile;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        $chart = ChartService::generateChart(Auth::user(), 'Comment');

        return view('user.dashboard', compact('chart'));
    }

    public function comments()
    {
        $comments = Comment::all();
        return view('user.comments', compact('comments'));
    }

    public function commentDelete($id)
    {
        $comment = Comment::where('id', $id)->where('user_id', Auth::id())->first();
        if($comment) {
            $comment->delete();
        }
        return back();
    }

    public function profile()
    {
        return view('user.profile');
    }

    public function profilePost(ProfileUpdateRequest $request)
    {
        $user = Auth::user();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        if($request->password != '') {
            if(!(Hash::check($request->password, Auth::user()->password))) {
                return back()->with('error', 'Your current password does not match with the password you provided');
            }
            if(strcmp($request->password, $request->new_password) == 0) {
                return back()->with('error', 'New password cannot be same as your current password');
            }
            $validation = $request->validate([
                'password' => 'required',
                'new_password' => 'required|string|min:6|confirmed'
            ]);

            $user->password = bcrypt($request->password);
            $user->save();

            return back()->with('success', 'Password has been changed successfully');
        }

        return back()->with('success', 'Profile has been updated successfully');
    }

    public function store(Request $request)
    {
        $comment = new Comment();

        $comment->post_id = $request['post_id'];
        $comment->user_id = Auth::id();
        $comment->content = $request['content'];
        $comment->save();

        return back();
    }
}
