<?php

namespace App\Http\Controllers\User;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    public function index()
    {
        $posts = Post::paginate(5);
        return view('welcome', compact('posts'));
    }

    public function singlePost(Post $post)
    {
        return view('single-post', compact('post'));
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

    public function contactForm()
    {

    }
}
