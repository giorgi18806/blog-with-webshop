<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(!Auth::check()) {
            return redirect()->route('home');
        }elseif (Auth::user()->$role != true) {
            return redirect()->route('home');
        }
        return $next($request);
    }
}
